db.auth({ user: "{{ _mongo_root_user_ }}" , pwd: "{{ _mongo_root_passwd_ }}" })
db.createUser( { user: "{{ _mongo_user_ }}" , pwd: "{{ _mongo_user_passwd_ }}" , roles: [ { role: "readWrite", db  : "{{ _mongo_db_ }}" } ] })
