![](https://hevodata.com/blog/wp-content/uploads/2017/09/mn.png)
           
# NodeJs - MongoDB

## Table of Content
- [NodeJS MongoDB POC](#nodejs-mongodb-poc)
  - [Table of Content](#table-of-content)
  - [About this project](#about-this-project)
  - [Step Cero (0)](#step-cero-0)
  - [What does this project do?](#what-does-this-project-do)
    - [Details of this project](#details-of-this-project)
  - [How is this work](#how-is-this-work)
    - [Structure Detail](#structure-detail)
  - [How to Configure the environment](#how-to-configure-environment)
  - [Build the environment](#build-the-environment)
  - [Workaround](#workaround)
  - [After Build The Environment ](#after-build-the-environment)

## About this project
```
.
├ README.md
├ cloudInfra
│   ├ ec2-instance
│   │   ├ minik8s.tf
│   │   ├ mongodb.tf
│   │   ├ output.tf
│   │   └ provider.tf
│   ├ modules
│   │   ├ amazon
│   │   │   └ createAmi
│   │   │       ├ ami-aws.tf
│   │   │       ├ main.tf
│   │   │       ├ output.tf
│   │   │       └ variables.tf
│   │   ├ createSG
│   │   │   ├ main.tf
│   │   │   ├ output.tf
│   │   │   └ variables.tf
│   │   ├ createSSHKey
│   │   │   ├ main.tf
│   │   │   ├ output.tf
│   │   │   └ variables.tf
│   │   └ ubuntu
│   │       └ createAmi
│   │           ├ ami-ubuntu.tf
│   │           ├ main.tf
│   │           ├ output.tf
│   │           └ variables.tf
│   └ provider.tf
├ cloudManager
│   ├ ansible.cfg
│   ├ customs.yml
│   └ roles
│       ├ minik8s
│       │   ├ defaults
│       │   │   └ main.yml
│       │   ├ files
│       │   │   └ bibliotheca-deploy.j2
│       │   └ tasks
│       │       └ main.yml
│       └ mongodb
│           ├ defaults
│           │   └ main.yml
│           ├ files
│           │   ├ bibliotheca-deploy.j2
│           │   ├ docker-compose.j2
│           │   ├ init-mongo.js
│           │   └ mongod.conf
│           └ tasks
│               ├ docker.yml
│               ├ linux.yml
│               └ main.yml
└ sourceNodejsApi
    ├ Dockerfile
    ├ app.js
    ├ controllers
    │   └ product.controller.js
    ├ database
    │   └ index.js
    ├ models
    │   └ product.model.js
    ├ package-lock.json
    ├ package.json
    └ routes
        └ product.route.js

```

## Step Cero (0)

Create a **ssh key** and **aws access key**, because are part of this building process.

## What does this project do

This project will provisioner a simulate infraestructure that will expose an **api** REST in a **cloud** environmment using **mongodb** as a outside database in **aws cloud**.

For acomplish this the [infrastructure](cloundInfra/) will be deploy with [terraform](https://terraform.io) and will [provisioner](cloudManager/) the [api](sourceNodejsApi/) with [ansible](https://www.ansible.com/). 


### Details of this project

This project was devide by three (3) sub-modules: 

* [Application](sourceNodejsApi/) 
* [Provisioner](cloudManager/)
* [Infrastructure](cloundInfra/) 

The aplication will be a nodejs REST [api](sourceNodejsApi/) docker images, that reside on this [gitlab registry](registry.gitlab.com/ejos/k8s-nodejs-mongodb) project and it will run as a **pods** on a simulate **k8s** cluster with [minikube](https://kubernetes.io/docs/setup/learning-environment/minikube/) [provisioned](cloudManager/) on the whole with **mongodb** database in automotion way at the same time that the [infrastructure](cloundInfra/) is built it.

## How is this work

The [infrastructure](cloundInfra/) module has two (2) *tf* files: the [mongodb.tf](cloudInfra/ec2-instance/mongodb.tf) file, that will build the instance where the *database* will be place it and the [minik8s.tf](cloudInfra/ec2-instance/minik8s.tf) file, that will be the instance where the cluster will be locate it.

Both [mongodb.tf](cloudInfra/ec2-instance/mongodb.tf) and [minik8s.tf](cloudInfra/ec2-instance/minik8s.tf) while they are build it as a instance will [provisioned](cloudManager/) with the application that will reside on them: 

* The [minik8s.tf](cloudInfra/ec2-instance/minik8s.tf) will provisioned the [minikube]( roles/minik8s/tasks/main.yml ) cluster where the [api](sourceNodejsApi/) will run as a pods.
* The [mongodb.tf](cloudInfra/ec2-instance/mongodb.tf) will provisioned [mongodb]( roles/minik8s/tasks/main.yml ) docker image using *docker compose tools*. 

### Structure Detail

* [Application](sourceNodejsApi/): 
This REST **api** was build it as a **Basic CRUD** operations and it work as a design patter MVC reason why it will not covert the *views*, the *database uri* it will be set as a *env* called *MONGO_URI*.

* [Provisioner](cloudManager/): 
There were build with two (2) roles:
  * The [mongodb](roles/mongodb/tasks/linux.yml) role contains two (2) sub-playbooks: the [linux](roles/mongodb/tasks/linux.yml) playbook that will going to **setup** the *MONGO_URI* variable for [k8s-deploy.yml](/roles/mongodb/files/bibliotheca-deploy.j2), that it necesary for the *pod* establish the connection with *mongodb*, and the [docker](roles/mongodb/tasks/docker.yml) playbook that will install *docker* support.

  * The [minik8s](roles/minik8s/tasks/main.yml) role that will provide **minikube** support and will apply the [k8s-deploy.yml](/roles/minik8s/files/bibliotheca-deploy.j2) using **kubectl**.

* [Infrastructure](cloundInfra/): 

It was built it as a [module](cloudInfra/modules), is a directory that contain a simple way to work, in this case there are tree (3) modules, but one was splited by two (2) because the **ami** type, one for **amazon** and the other one for **ubuntu**, those modules will be describe as follow:

* [createAmi](modules/amazon/main.tf) That will create a instance and inmediatly it will provisioner with ansible the type of *environment* that it has been created for example: *ubuntu* for *mongodb* and *amazon* for *minikube*, its variables are [here](cloudInfra/modules/amazon/variable.tf).

* [createSG](modules/createSG/main.tf) That will create *security groups*, its variables are [here](cloudInfra/modules/createSG/variable.tf).

* [createSSHkey](modules/createSSH/main.tf) Will upload your **ssh public key** to *aws* so it will be pre-loaded into the instance at the time that its built, its variable are [here](cloudInfra/modules/createSSH/main.tf).

Then there are a simplified files that at the end it will build the entire *environment*:

  * [provider.tf](cloudInfra/ec2-instance/provider.tf) That will inform to **terraform** what plugging it will need (aws in this case).
  * [mongodb.tf](cloudInfra/ec2-instance/mongodb.tf) That will build the **instance** that will run **mongodb** as a **container**
  * [minik8s.tf](cloudInfra/ec2-instance/minik8s.tf) In this case it will build the **instance** that will simulate a **k8s** cluster using [minikube](https://kubernetes.io/docs/setup/learning-environment/minikube/)
  * [output.tf](cloudInfra/ec2-instance/output.tf) Here it will display the **ip public address** for each **instance**


## How to Configure the Environment

* [Provisioner](cloudManager/): 

  * [users:]( cloudManager/roles/mongodb/vars/main.yml) it will allow that user to run **docker** command without been **sudo** 
  * [mongo_user:](cloudManager/roles/mongodb/defaults/main.yml) This will be the **user** owner of the database
  * [mongo_user_passwd:](cloudManager/roles/mongodb/defaults/main.yml) This must be the **password** associated to *mongo user*
  * [mongo_db_mongodb:](cloudManager/roles/mongodb/defaults/main.yml) This will be the **database** name
  * [mongo_root_user:](cloudManager/roles/mongodb/defaults/main.yml) This will be the **root** user
  * [mongo_root_passwd:](cloudManager/roles/mongodb/defaults/main.yml) This must be the **password** associated to *root user*

  All the files that **mongodb** need for *run* are located in the *ec2 instance* in: /srv .

  *Example:*
```
users:
  - name: jonhDoe <- 'ubuntu' if the ec2 is ubuntu or 'ec2-user' if the ec2 is amazon
```
**Note:** The user **jonhDoe** must exist in the remote host

* [Infraestructure](cloudInfra/): 

  * [provider.tf](cloudInfra/ec2-instance/provider.tf): Replace the credentials 
  * [mongodb.tf](cloudInfra/ec2-instance/mongodb.tf): Replace the * _private_key_path_ =   "<< PATH OF YOUR SSH PRIVATE KEY >>"*
  * [minik8s.tf](cloudInfra/ec2-instance/minik8s.tf): Replace not just the * _private_key_path_ =   "<< PATH OF YOUR SSH PRIVATE KEY >>"* variable but the *_ssh_public_key_   =  "<< YOUR SSH PUBLIC KEY >>"*.

## Build The Environment
``` 
 cd cloudInfra/ec2-instances && terraform init && terraform plan 
```

If everything is alright

``` 
terraform apply -auto-approve

```
## Workaround

* For some reason the **docker-compose** does not *pre-create* either the **mongo_user** nor **database**, even though the [mongo-init](/roles/mongodb/files/init-mongo.js) is *pre-loaded*, so you can **load it** manually.

So you must stablish a **ssh** connection on the **mongodb** 

``` 
ssh -i ~/_your_ssh_private_key -l _ec2_user_ _my_ami_minik8s_public_ip_  -L 8080:localhost:31737

```
Get into the **docker** execute the commando as follow:

```
 docker exec -ti mongodb sh 

 mogodb 127.0.0.1:27017 
 use admin
```
Copy and paste the line as follow: 

```
db.auth({ user: "{{ _mongo_root_user_ }}" , pwd: "{{ _mongo_root_passwd_ }}" })
db.createUser( { user: "{{ _mongo_user_ }}" , pwd: "{{ _mongo_user_passwd_ }}" , roles: [ { role: "readWrite", db  : "{{ _mongo_db_ }}" } ] })

```

## After Build The Environment
 
Once the *infrastructure* was built it create a **ssh tunnel** for work with the instance early create it 

``` 
terraform output

my_ami_minik8s_public_ip = [
  "1.2.3.4",
]
my_ami_mongo_public_ip = [
  "5.6.7.8",
]

```

Now access to *minik8s* instance for get the **LoadBalancer** Rest **api port** 

``` 
ssh -i ~/_your_ssh_private_key -l _ec2_user_ _my_ami_minik8s_public_ip_ 
```
Once you get into the *ec2*, logged as a **root**

``` 
sudo -i 

```
Now get the **LoadBalancer** ip port

``` 
kubectl get svc

NAME  TYPE           CLUSTER-IP     EXTERNAL-IP    PORT(S)          AGE
app   LoadBalancer   10.11.12.13    14.15.16.18    3000:31737/TCP   Xh

```

Now stablish the tunnel with *minik8s* instance, get out from the *instance* and on the pc run:

``` 
ssh -i ~/_your_ssh_private_key -l _ec2_user_ _my_ami_minik8s_public_ip_  -L 8080:localhost:31737

```

You can check if everything is alright, on the browser:

``` 
http://localhost:8080/products/test

```
Use **Postman** for to test the **CRUD REST api** example: 

  - 'localhost:8080/products/create'
  - 'localhost:8080/products/PRODUCT_ID'
  - 'localhost:8080/products/PRODUCT_ID/update'
  - 'localhost:8080/products/PRODUCT_ID/delete'
