resource "aws_instance" "ami-instance" {
  ami             = "${data.aws_ami.aws.id}"
  instance_type   = "${var._instance_type_}"

  key_name        = "${var._ssh_key_name_}"

  security_groups = ["${var._sg-acl-id_}"]

  associate_public_ip_address = "true"

  tags = {
    Name = "${var._tag_name_}"
  }

  provisioner "remote-exec" {
    inline = [ 
               "sudo yum install -y epel-release ",
               "sudo yum install -y python3-pip",
               "sudo pip3 -y install docker"
             ]

    connection {
      agent       = "false"
      host        = "${self.public_ip}"
      type        = "ssh"
      user        = "${var._user_}"
      private_key = "${file(var._private_key_path_)}"
    }
  }

  provisioner "local-exec" {
    working_dir = "${var._working_dir_}"
    command = "ansible-playbook -u ${var._user_} -i '${self.public_ip},' --private-key ${var._private_key_path_} customs.yml --tags ${var._tag_ansible_} "
  }

}
