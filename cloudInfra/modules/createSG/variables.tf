
variable "_vpc_id_" {
  type = "string"
  description = "VPC ID"
}

variable "_sg_name_" {
  type = "string"
  description = "Security Group Name"
}

variable "_tcp_port_" {
  type = "string"
  description = "Service Port eg: '22'"

}
