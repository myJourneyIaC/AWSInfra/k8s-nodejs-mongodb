//network
resource "aws_security_group" "ingress-all" {
  name = "${var._sg_name_}"
  vpc_id = "${var._vpc_id_}"
  ingress {
      cidr_blocks = [
        "0.0.0.0/0"
      ]

     from_port = "${var._tcp_port_}"
     to_port   = "${var._tcp_port_}"
     protocol  = "tcp"
  }

  egress {
     from_port = 0
     to_port = 0
     protocol = "-1"
     cidr_blocks = ["0.0.0.0/0"]
   }
}
