
module "new-ssh-key" {

   source         = "../modules/createSSHKey"

   _ssh_key_name_     =  "devops-ssh-key"
   _ssh_public_key_   =  "<< YOUR SSH PUBLIC KEY >>"

}

module "new-sg" {

   source         = "../modules/createSG"

   _vpc_id_       = "vpc-7487801c"
   _sg_name_      = "ssh-sg"
   _tcp_port_     = "22" 

}

module "ami-k8s" {

   source             =  "../modules/amazon/createAmi"

   _ssh_key_name_     =  "${module.new-ssh-key.key_name}"
   _tag_name_         =  "minik8s-devops"
   _sg-acl-id_        =  "${module.new-sg.sg}"
   _user_             =  "ec2-user"
   _private_key_path_ =  "<< PATH OF YOUR SSH PRIVATE KEY >>"
   _tag_ansible_      =  "minik8s"
   _working_dir_      =  "../../cloudManager"

}
